import React, { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Home from "./components/Home";
import "./App.css";
import List from "./components/List.jsx";
import { SnackbarProvider } from "notistack";
import PageNotFound from "./components/PageNotFound.jsx";

function App() {
  return (
    <SnackbarProvider maxSnack={3} anchorOrigin={{ vertical: 'top', horizontal: 'center'}}>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to="/boards" replace />} />
        <Route path="/boards" element={<Home />} />
        <Route path="/boards/:id" element={<List />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
    </SnackbarProvider>
  );
}

export default App;
