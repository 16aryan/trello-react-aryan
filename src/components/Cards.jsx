import React, { useEffect, useState } from "react";
import { Stack, Typography } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import CheckList from "./CheckList";
import { fetchingCards, handlingAddCards, handlingDeleteCards } from "./API/api";
import { useSnackbar } from "notistack";
import AddComponent from "./AddComponent";

function Cards({ id, cardName, setcardName }) {
  const [open, setOpen] = useState(false);
  const [fetchedCardsDet, setFetchedCardDet] = useState([]);
  const [selectedId, setselectedId] = useState("");
  const [showInput, setShowInput] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const fetchCards = async () => {
    try {
      const response = await fetchingCards(id);
      setFetchedCardDet(response);
      
    } catch (error) {
      console.error("Error fetching cards:", error);
      enqueueSnackbar("Failed to fetch cards"+error.message, { variant: "error" });
    }
  };

  useEffect(() => {
    fetchCards();
  }, []);

  const handleDelete = async (cardId) => {
    try {
      await handlingDeleteCards(cardId);
      const updatedCards = fetchedCardsDet.filter((card) => card.id !== cardId);
      setFetchedCardDet(updatedCards);
      enqueueSnackbar("Card deleted successfully", { variant: "success" });
    } catch (error) {
      console.error("Error deleting card:", error);
      enqueueSnackbar("Failed to delete card", { variant: "error" });
    }
  };

  const handleButtonClick = () => {
    setShowInput(false);
    setShowInput(true);
  };

  const handleAddButtonClick = async (e) => {
    e.preventDefault();
    try {
      if (cardName === "") {
        enqueueSnackbar("Please provide valid card Name", { variant: "error" });
        console.error("Please enter a valid card name");
      } else {
        const res = await handlingAddCards(id, cardName);
        setcardName("");
        setFetchedCardDet([res, ...fetchedCardsDet]);
        enqueueSnackbar("Card added successfully", { variant: "success" });
        setShowInput(false);
      }
    } catch (error) {
      console.log("Error in posting card:", error);
      enqueueSnackbar("Failed to add card", { variant: "error" });
    }
  };

  const handleClick = (event) => {
    setOpen(true);
    const ids = event.target.id;
    setselectedId(ids);
  };

  return (
    <>
      <Stack
        justifyContent={"center"}
        alignItems={"center"}
        sx={{
          flexDirection: "column",
          background: "#b3c3c3",
          color: "grey",
          borderRadius: "5px",
        }}
      >
        {fetchedCardsDet.map((data) => (
          <Stack
            key={data.id}
            sx={{
              marginBottom: "5px",
              marginTop: "5px",
              flexDirection: "row",
              alignItems: "center",
              width: "15vw",
              justifyContent: "space-evenly",
              background: "white",
              borderRadius: "5px",
              cursor: "pointer",
            }}
          >
            <Typography
              id={data.id}
              sx={{ width: "10vw" }}
              onClick={handleClick}
            >
              {data.name}
            </Typography>
            <IconButton
              onClick={() => handleDelete(data.id)}
              aria-label="delete"
              color="error"
            >
              <DeleteIcon />
            </IconButton>
          </Stack>
        ))}
        <CheckList open={open} setOpen={setOpen} selectedId={selectedId} />
      </Stack>

      <Stack direction="row" alignItems="center" justifyContent="center" sx={{width:'100%'}}>
        {showInput ? (
          <>
      
              <AddComponent placeholder={'enter Card Name'} Value={cardName} change={(e) => setcardName(e.target.value)} create={'Card'} Submit={handleAddButtonClick}/>
          </>
        ) : (
          <Stack sx={{ justifyContent: "center" }}>
            {" "}
            <IconButton onClick={handleButtonClick}>
              <AddIcon />
            </IconButton>
          </Stack>
        )}
      </Stack>
    </>
  );
}

export default Cards;
