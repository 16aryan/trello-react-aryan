import React, { useState, useEffect } from "react";
import {Stack} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import LinearWithValueLabel from "./LinearWithValueLabel";
import {
  addingItem,
  fetchCheckItem,
  handlingDeleteItem,
  toggler,
} from "./API/api";

import AddComponent from "./AddComponent";
import { useSnackbar } from "notistack";

function CheckItem({
  selectedId,
  id,
  data,
  showAdditem,
  setShowAddItem,
}) {
  const [newItem, setItem] = useState("");
  const [checkItemData, setCheckItem] = useState([]);
  const [checked, setChecked] = useState(1);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetchCheckItem(id);
        setCheckItem(res);
        let data = res;
        setChecked(data.filter((item) => item.state === "complete").length);
        setChecked(checked + 1);
      } catch (error) {
        console.log("error in saving checkItem", error);
      }
    };

    fetchData();
  }, []);

  const checkItemName = (event) => {
    setItem(event.target.value);
  };
  const handleCheckItemSubmit = async (e) => {
    try {
      e.preventDefault();
      if (newItem) {
        const response = await addingItem(id, newItem);

        setItem("");
        setCheckItem((prevCheckItemData) => {
          const updatedCheckItemData = [...prevCheckItemData, response];
          return updatedCheckItemData;
        });
        enqueueSnackbar("Check item added successfully", {
          variant: "success",
        });
        setShowAddItem(false);
      } else {
        enqueueSnackbar("Please enter a valid check item name", {
          variant: "error",
        });
        setShowAddItem(false);
      }
    } catch (error) {
      console.log("error in posting checkItem", error);
      enqueueSnackbar("Failed to add check item", { variant: "error" });
    }
  };

  const handleDeleteItem = async (itemId) => {
    try {
      await handlingDeleteItem(id, itemId);
      setCheckItem(checkItemData.filter((data) => data.id !== itemId));
      enqueueSnackbar("Check item deleted successfully", {
        variant: "success",
      });
    } catch (error) {
      console.log("error", error);
      enqueueSnackbar("Failed to delete check item", { variant: "error" });
    }
  };

  const toggleCheckItem = async (itemId, CState, event) => {
    const State = event.target.checked ? "complete" : "incomplete";


    try {
      setChecked((prevChecked) =>
        event.target.checked ? prevChecked + 1 : prevChecked - 1
      );
      await toggler(selectedId, itemId, State);
    setCheckItem((prevCheckItemData) => {
      const updatedCheckItemData = prevCheckItemData.map((data) => {
        if (data.id === itemId) {
          return { ...data, state: State };
        }
        return data;
      });
      return updatedCheckItemData;
    });
    } catch (error) {
      console.error("Error toggling check item state:", error);
      enqueueSnackbar("Failed to toggle check item state", {
        variant: "error",
      });
    }
  };

  let progress;
  const progressPercentage = (checkItemData) => {
    try {
      if (checkItemData && checkItemData.length > 0) {
        const totalItems = checkItemData.length;
        const completedItemsCount = checkItemData.filter(
          (item) => item.state === "complete"
        ).length;
        progress = (completedItemsCount / totalItems) * 100;
      }
    } catch (error) {
      console.log("error in finding progress percentage", error);
    }
  };
  progressPercentage(checkItemData);

  return (
    <>
      {checkItemData.length > 0 ? (
        <LinearWithValueLabel progress={progress} sx={{ width: "100%" }} />
      ) : null}

      <Stack
        flexDirection="column"
        alignItems={"center"}
        width={"12vw"}
        justifyContent={"space-between"}
        sx={{ alignItems: "left" }}
      >
        {checkItemData.map((item) => (
          <li
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              listStyle: "none",
              margin: "10px",
              paddingRight: "50px",
              width: "15vw",
              justifyContent: "space-around",
            }}
            key={item.id}
          >
            <Stack
              sx={{
                display: "flex",
                alignItems: "center",
                listStyle: "none",
                margin: "10px",
                paddingRight: "50px",
                width: "30vw",
                justifyContent: "space-around",
                flexDirection: "row",
              }}
            >
              <input
                type="checkbox"
                checked={item.state === "complete"}
                onChange={() => toggleCheckItem(item.id, item.state, event)}
              />

              <label
                style={{
                  marginLeft: "5px",
                  textDecoration:
                    item.state === "complete" ? "line-through" : "none",
                }}
              >
                {item.name}
              </label>
              <IconButton
                onClick={() => handleDeleteItem(item.id)}
                aria-label="delete"
                color="error"
                sx={{ width: "5vw" }}
              >
                <DeleteIcon />
              </IconButton>
            </Stack>
          </li>
        ))}
      </Stack>
      {/* <AddCheckItem
        sendCheckItem={handleCheckItemSubmit}
        checkItemName={checkItemName}
        newItem={newItem}
      /> */}

  <AddComponent
    placeholder={"enter CheckItem"}
    Value={newItem}
    change={checkItemName}
    create={"CheckItem"}
    Submit={handleCheckItemSubmit}
  />


    </>
  );
}

export default CheckItem;
