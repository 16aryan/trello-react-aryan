import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Stack,
  Typography,
  CircularProgress,
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Cards from "./Cards";
import Loader from './Loader'

import { fetchList, postingList, handlingDelete } from "./API/api";
import { useSnackbar } from "notistack";
import AddComponent from "./AddComponent";

function List() {
  const { id } = useParams();
  const { enqueueSnackbar } = useSnackbar();
  const [fetchedList, setFetchedList] = useState([]);
  const [ListVal, setListVal] = useState("");
  const [cardName, setcardName] = useState("");
  const [Loading, setLoading] = useState(true);
 
  const getAllList = async () => {
    try {
      const response = await fetchList(id);
      setFetchedList(response);
      enqueueSnackbar("Successfully fetched all List", { variant: "success" });
    } catch (error) {
      console.error("Error fetching data:", error);
      enqueueSnackbar("Failed to fetch all List"+error.message, { variant: "error" });
    }
    setLoading(false);
  };

  useEffect(() => {
    getAllList();
  }, []);

  const handleInput = (event) => {
    setListVal(event.target.value);
  };

  const addList = async (e) => {
    try {
      e.preventDefault();
      if (ListVal) {
        const res = await postingList(id, ListVal);
        console.log("added successful");
        setListVal("");
        setFetchedList((prevList) => [...prevList, res]);
        enqueueSnackbar("List added successfully", { variant: "success" });
      } else {
        enqueueSnackbar("invalid list value", { variant: "error" });
      }
    } catch (error) {
      console.log("error in adding List", error);
      enqueueSnackbar("Failed to add List"+error.message, { variant: "error" });
    }
  };

  const handleDelete = async (listid) => {
    try {
      const deleteRes = await handlingDelete(listid);
      console.log("delete successful");
      setFetchedList((prevList) =>
        prevList.filter((list) => list.id !== listid)
      );
      enqueueSnackbar("List deleted successfully", { variant: "success" });
    } catch (error) {
      console.log("error in deleting ", error);
      enqueueSnackbar("Failed to delete List", { variant: "error" });
    }
  };

  return (
    <div
      style={{
        backgroundImage: `linear-gradient(to top, #FDFCFB, #E2D1C3)`,
        height: "100vh",
      }}
    >
      <Typography
        sx={{
          textAlign: "center",
          paddingTop: "5vh",
          color: "grey",
          fontSize: "30px",
        }}
      >
        Board
      </Typography>
      <Link to="/" style={{ textDecoration: "none" }}>
        <Button
          variant="contained"
          sx={{ marginLeft: "90%", marginTop: "-4%" }}
        >
          Back
        </Button>
      </Link>
      {Loading ? (
      <Loader />
      ) : (
        <Stack
          flexDirection={"row"}
          sx={{
            marginTop: "10vh",
            overflow: "auto",
            height: "100vh",
            marginRight: "5vw",
          }}
        >
          {fetchedList.map((data) => (
            <Box
              key={data.id}
              sx={{
                marginLeft: "5vw",
                backgroundImage: "linear-gradient(to right, #b0b7be, #86908b)",
                width: "20vw",
                borderRadius: "5px",
                height: "fit-content",
                boxSizing: "border-box",
                overflowY: "none",
              }}
            >
              <Stack flexDirection={"row"} justifyContent={"space-evenly"}>
                <Typography
                  sx={{
                    marginLeft: "15px",
                    marginTop: "15px",
                    width: "20vw",
                    color: "white",
                  }}
                >
                  {data.name}
                </Typography>
                <IconButton
                  onClick={() => handleDelete(data.id)}
                  sx={{ marginTop: "5px" }}
                  aria-label="delete"
                  color="error"
                >
                  <DeleteIcon />
                </IconButton>
              </Stack>
              <Cards
                id={data.id}
                
                cardName={cardName}
                setcardName={setcardName}
              />
            </Box>
          ))}
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                marginLeft: "5vw",
                backgroundImage: "linear-gradient(to right, #b0b7be, #86908b)",
                width: "30vw",
                borderRadius: "5px",
                height: "max-content",
                justifyContent: "space-evenly",
              }}
            >
              {/* <Typography
                sx={{
                  fontSize: "20px",
                  marginTop: "15px",
                  marginBottom: "15px",
                  color: "white",
                }}
              >
                Add new List
              </Typography>
              <Textarea
                color="primary"
                minRows={2}
                value={ListVal}
                placeholder="Enter new List"
                size="sm"
                variant="soft"
                onChange={handleInput}
                sx={{
                  height: "fit-content",
                  margin: "0 2rem",
                  width: "max-content",
                }}
              />
              <Button
                type="submit"
                variant="contained"
                size="small"
                sx={{ margin: "15px  0 15px 0" }}
              >
                Add List
              </Button> */}
              <AddComponent
                placeholder={"enter List Name"}
                Value={ListVal}
                change={handleInput}
                create={"List"}
                Submit={addList}
              
              />
            </Box>
          
        </Stack>
      )}
    </div>
  );
}

export default List;
