import axios from "axios";

const apiKey = import.meta.env.VITE_REACT_APP_TRELLO_API_KEY;
const apiToken = import.meta.env.VITE_REACT_APP_TRELLO_API_TOKEN;

axios.defaults.params = {
  key: apiKey,
  token: apiToken,
};

export const addingBoards = async (boardName) => {
  const response = await axios.post("https://api.trello.com/1/boards", {
    name: boardName,
  });
  return response.data;
};

export const getBoardsData = async () => {
  const response = await axios.get(
    "https://api.trello.com/1/members/me/boards"
  );
  return response.data;
};

export const handleDeleteBoards = async (id) => {
  const response = await axios.delete(
    `https://api.trello.com/1/boards/${id}`
  );
  return response;
};

export const fetchList = async (id) => {
  const response = await axios.get(
    `https://api.trello.com/1/boards/${id}/lists`
  );
  return response.data;
};

export const postingList = async (id, ListVal) => {
  const res = await axios.post(
    `https://api.trello.com/1/boards/${id}/lists?name=${ListVal}`
  );
  return res.data;
};

export const handlingDelete = async (listid) => {
  const deleteRes = await axios.put(
    `https://api.trello.com/1/lists/${listid}/closed?key=${apiKey}&token=${apiToken}&value=true`
  );
};

export const fetchCheckList = async (selectedId) => {
  const res = await axios.get(
    `https://api.trello.com/1/cards/${selectedId}/checklists`
  );
  return res.data;
};

export const handlingDeleteCheckList = async (id) => {
  await axios.delete(`https://api.trello.com/1/checklists/${id}`);
};

export const addingCheckList = async (selectedId, enterd) => {
  const response = await axios.post(
    `https://api.trello.com/1/cards/${selectedId}/checklists?key=${apiKey}&token=${apiToken}&name=${enterd}`
  );
  return response.data;
};

export const fetchCheckItem = async (id) => {
  const res = await axios.get(
    `https://api.trello.com/1/checklists/${id}/checkItems?`
  );
  return res.data;
};

export const addingItem = async (id, newItem) => {
  const response = await axios.post(
    `https://api.trello.com/1/checklists/${id}/checkItems?name=${newItem}`
  );
  return response.data;
};

export const handlingDeleteItem = async (id, itemId) => {
  const response = await axios.delete(
    `https://api.trello.com/1/checklists/${id}/checkItems/${itemId}`
  );
  return response.data;
};

export const toggler = async (selectedId, itemId, State) => {
  const res = await axios.put(
    `https://api.trello.com/1/cards/${selectedId}/checkItem/${itemId}?key=${apiKey}&token=${apiToken}&state=${State}`
  );
};

export const fetchingCards = async (id) => {
  const response = await axios.get(
    `https://api.trello.com/1/lists/${id}/cards?`
  );
  return response.data;
};

export const handlingDeleteCards = async (cardId) => {
  await axios.delete(`https://api.trello.com/1/cards/${cardId}?`);
};

export const handlingAddCards = async (listId, cardName) => {
  const res = await axios.post(
    `https://api.trello.com/1/cards?name=${cardName}&idList=${listId}`
  );
  return res.data;
};
