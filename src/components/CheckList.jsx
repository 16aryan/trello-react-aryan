import * as React from "react";
import { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Stack from "@mui/material/Stack";
import axios from "axios";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import CloseIcon from "@mui/icons-material/Close";
import AddComponent from "./AddComponent";
import { useSnackbar } from "notistack";

import CheckItem from "./CheckItem";
import {
  addingCheckList,
  fetchCheckList,
  handlingDeleteCheckList,
} from "./API/api";
import Loader from "./Loader";

export default function CheckList({ open, setOpen, selectedId }) {
  const [checkListData, setCheckListData] = useState([]);
  const [formOpen, setFormOpen] = useState(false);
  const [enterd, setEntered] = useState("");
  const { enqueueSnackbar } = useSnackbar();
  const myApiKey = import.meta.env.VITE_REACT_APP_TRELLO_API_KEY;
  const myToken = import.meta.env.VITE_REACT_APP_TRELLO_API_TOKEN;
  const [showAdditem, setShowAddItem] = useState(false);
  const [loading,setLoading] = useState(true);
  const handleCloseBox = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (selectedId) {
      const fetchData = async () => {
        try {
          const res = await fetchCheckList(selectedId);
          setCheckListData(res);
          setLoading(false);
        } catch (error) {
          console.error("Error fetching checklists:", error);
          enqueueSnackbar("failed to fetch Checklist ", { variant: "error" });
        }
      };

      fetchData();
    }
  }, [selectedId]);

  const handleFormOpen = () => {
    setFormOpen(true);
  };

  const handleFormClose = () => {
    setFormOpen(false);
  };

  if (!selectedId) {
    return null;
  }

  const handleChange = (event) => {
    setEntered(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDeleteCheckList = async (id) => {
    try {
      handlingDeleteCheckList(id);
      setCheckListData((prevData) => prevData.filter((item) => item.id !== id));
      enqueueSnackbar("Checklist deleted successfully", { variant: "success" });
      console.log("Deleting checkList successful");
    } catch (error) {
      console.log("Error in deleting ", error);
      enqueueSnackbar("Failed to delete checklist", { variant: "error" });
    }
  };

  const handlingSubmit = async (event) => {
    event.preventDefault();

    try {
      if (enterd) {
        const response = await addingCheckList(selectedId, enterd);
        setCheckListData([...checkListData, response]);
        setEntered("");
        setFormOpen(false);
        enqueueSnackbar("Checklist added successfully", { variant: "success" });
      } else {
        enqueueSnackbar("Please enter a valid checklist value", {
          variant: "error",
        });
      }
    } catch (error) {
      console.log("Error while posting checkList", error);
      enqueueSnackbar("Failed to add checklist", { variant: "error" });
    }
  };

  return (
    <React.Fragment>
      {loading ? (<Loader />):(
      <Dialog open={open} onClose={handleClose}>
        <Stack
          flexDirection={"row"}
          justifyContent={"space-between"}
          alignItems={"center"}
          sx={{
            backgroundImage: "linear-gradient(to right, #EE9CA7, #FFDDE1)",
            color: "rgb(185 144 144 / 87%",
            overflowY: "hidden",
          }}
        >
          <DialogTitle>Add Checklist </DialogTitle>
          <IconButton
            sx={{ marginRight: "20px" }}
            onClick={formOpen ? handleFormClose : handleFormOpen}
          >
            {formOpen ? <CloseIcon /> : "+"}
          </IconButton>
        </Stack>

        <DialogContent>
          {formOpen && (
            
            <AddComponent
              placeholder={"enter CheckList"}
              Value={enterd}
              change={handleChange}
              create={"CheckList"}
              Submit={handlingSubmit}
            />
          )}
          {checkListData.map((data) => (
            <DialogContent
              key={data.id}
              sx={{
                display: "flex",
                backgroundImage: `linear-gradient(to bottom, #FFECD2, #FCB69F)`,
                marginBottom: "10px",
                fontSize: "20px",
                flexDirection: "column",
                alignItems: "center",
                borderRadius: "10px",
                overflow: "hidden",
              }}
            >
              <Stack
                flexDirection={"row"}
                display={"flex"}
                justifyContent={"row"}
                alignItems={"center"}
                sx={{ width: "32vw", marginBottom: "10px" }}
              >
                <DialogContent
                  sx={{
                    cursor: "pointer",
                    width: "fit-content",
                    overflowY: "none",
                    padding: "0",
                  }}
                >
               
                  <Button   variant="outlined">+</Button>
                </DialogContent>
                <DialogContent
                  sx={{ width: "fit-content", overflowY: "none", padding: "0" }}
                >
                  {data.name}{" "}
                </DialogContent>
                <IconButton
                  onClick={() => handleDeleteCheckList(data.id)}
                  aria-label="delete"
                  color="error"
                  sx={{ width: "5vw", overflowY: "none" }}
                >
                  <DeleteIcon />
                </IconButton>
              </Stack>
              <CheckItem
                selectedId={selectedId}
                id={data.id}
                data={data}
                showAdditem={showAdditem}
                setShowAddItem={setShowAddItem}
              />
            </DialogContent>
          ))}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseBox}>Cancel</Button>
        </DialogActions>
      </Dialog>
      )}
    </React.Fragment>
  );
}
