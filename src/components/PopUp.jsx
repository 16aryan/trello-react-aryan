import React, { useState } from "react";
import Dialog from "@mui/material/Dialog";
import { addingBoards } from "./API/api";
import AddComponent from "./AddComponent";
import { useSnackbar } from "notistack";

function PopUp({ open, onClose, boardData, setBoardData }) {
  const [boardName, setBoardName] = useState("");
const {enqueueSnackbar}= useSnackbar();
  const addBoard = async (e) => {
    try {
      e.preventDefault();

      if (!boardName) {
        alert("invalied board Value");
      }

      const response = await addingBoards(boardName);
      console.log("Board created:", response);
      onClose();
      setBoardData([...boardData, response]);
      setBoardName("");
      enqueueSnackbar('Added  Boards', { variant:'success' });
      return response.data;
    } catch (error) {
      enqueueSnackbar('Failed to add Boards'+error.message, { variant:'error' });
      console.error("Error creating board:", error.message);
      throw error;
    }
  };

  const handleChange = (event) => {
    setBoardName(event.target.value);
  };

  return (
    <Dialog open={open} onClose={onClose} >
      {/* <form onSubmit={addBoard}>
        <DialogContent>
          <label>Enter board name</label>
          <TextField
            id="boardName"
            label="Board Name"
            variant="outlined"
            fullWidth
            value={boardName}
            sx={{
              marginTop: "1vh",
            }}
            onChange={handleChange}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={{ mt: 2 }}
          >
            Create Board
          </Button>
        </DialogContent>
      </form> */}
      <AddComponent placeholder={'enter board Name'} Value={boardName} change={handleChange} create={'Board'} Submit={addBoard}/>
    </Dialog>
  );
}

export default PopUp;
