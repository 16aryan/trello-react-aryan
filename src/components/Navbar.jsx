import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import PopUp from "./PopUp";

export default function Navbar({
  deleteAllBoards,
  boardData,
  setBoardData,
}) {
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const handleOpenDialog = () => {
    setIsDialogOpen(true);
  };

  const handleCloseDialog = () => {
    setIsDialogOpen(false);
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" sx={{ height: "10vh" }}>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Trello
          </Typography>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          ></IconButton>
          <Button color="inherit" onClick={handleOpenDialog}>
            Add Board
          </Button>
          <PopUp
            open={isDialogOpen}
            onClose={handleCloseDialog}
            boardData={boardData}
            setBoardData={setBoardData}
          />
          <Button color="inherit" onClick={deleteAllBoards}>
            Delete All
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
