import {
  Button,
  Stack,
  DialogContent,
  DialogTitle,
  IconButton,
} from "@mui/material";
import React, { useState } from "react";
import TextField from "@mui/material/TextField";

function AddComponent({ placeholder, Value, change, create, Submit }) {
  const [openForm, setOpenForm] = useState(false);

  const handleToggleForm = () => {
    setOpenForm(!openForm);
  };

  return (
    <div>
      {openForm ? (
        <form onSubmit={Submit}>
          <DialogContent sx={{ minWidth: "15vw" }}>
            <TextField
              value={Value}
              variant="outlined"
              fullWidth
              onChange={change}
              sx={{
                marginTop: "1vh",
                background: "white",
                borderRadius: "10px",
                width: "100%",
              }}
              placeholder={placeholder}
            />
            <Stack>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                sx={{ mt: 2 }}
              >
                Create {create}
              </Button>
              <Button variant="outlined" onClick={() => setOpenForm(!openForm)}>
                cancel
              </Button>
            </Stack>
          </DialogContent>
        </form>
      ) : (
        <Button onClick={handleToggleForm} sx={{ width: "10vw" }}>
          Add {create}
        </Button>
      )}
    </div>
  );
}

export default AddComponent;
