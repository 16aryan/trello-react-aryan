import * as React from 'react';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

export default function BasicAlerts({error}) {
  return (
    <Stack sx={{ width: '100%'}} spacing={10} >
      <Alert severity="error">{error}</Alert>
    </Stack>
  );
}
