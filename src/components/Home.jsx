import React, { useEffect, useState } from "react";
import {
  Box,
  Stack,
  Typography,
} from "@mui/material";
import {  useSnackbar } from "notistack";
import NoDataFound from "./NoDataFound";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";
import { getBoardsData, handleDeleteBoards } from "./API/api";
import BasicAlerts from "./BasicAlerts";
import Loader from "./Loader";
function Home() {
  const [boardData, setBoardData] = useState([]);
  const [load, setisLoading] = useState(true);
  const [error, setError] = useState("");
  const { enqueueSnackbar } = useSnackbar();
  const fetchData = async () => {
    try {
      const response = await getBoardsData();
      setBoardData(response);
      setisLoading(false);
      setError("");
      enqueueSnackbar("Successfully fetched all boards", {
        variant: "success",
      });
    } catch (error) {
      enqueueSnackbar("Failed to fetch Boards" + error.message, {
        variant: "error",
      });
    
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  const deleteAllBoards = async () => {
    try {
      console.log("Hello");
      await Promise.all(
        boardData.map(async (board) => {
          const response = handleDeleteBoards(board.id);
      console.log("Board deleted:", response.data);
        })
      );
      setBoardData([]);
      enqueueSnackbar("Successfully Deleted all boards", {
        variant: "success",
      });
    } catch (error) {
     console.error("Error deleting boards:" + error.message, error);
      enqueueSnackbar("Failed to Delete Boards" + error.message, {
        variant: "error",
      });
    }
  };
  return (
    <>
     
      {error && error.length > 0 ? (
        <BasicAlerts error={error} />
      ) : (
        <>
          <Navbar
            deleteAllBoards={deleteAllBoards}
            boardData={boardData}
            setBoardData={setBoardData}
          />
          <Stack direction="column">
            <Typography
              align="center"
              sx={{
                marginTop: "5vh",
                fontSize: "30px",
                color: "grey",
                marginBottom: "5vh",
              }}
            >
              Your Boards
            </Typography>

            {load ? (
             <Loader />
            ) : (
              <Stack
                direction={{ xs: "column", sm: "row" }}
                justifyContent="flex-start"
                sx={{
                  marginTop: { xs: "4vh", sm: 0 },
                  marginLeft: "5vw",
                  marginRight: "10vh",
                  flexWrap: "wrap",
                  height: "50vh",
                }}
              >
                {boardData.length > 0 ? (
                  boardData.map((data) => (
                    <Link
                      key={data.id}
                      to={`/boards/${data.id}`}
                      style={{ textDecoration: "none" }}
                    >
                      <Box
                        key={data.id}
                        sx={{
                          p: 2,
                          background:
                            "linear-gradient(to right, #c9d6ff, #e2e2e2)",
                          color: "grey",
                          width: { xs: "100%", sm: "10vw" },
                          height: "5vh",
                          borderRadius: "10px",
                          margin: "0 20px 10px 0",
                          boxShadow: "0 4px 10px rgba(0, 0, 0, 0.1)",
                        }}
                      >
                        {data.name}
                      </Box>
                    </Link>
                  ))
                ) : (
                  <NoDataFound />
                )}
              </Stack>
            )}
          </Stack>
        </>
      )}
    </>
  );
}

export default Home;
