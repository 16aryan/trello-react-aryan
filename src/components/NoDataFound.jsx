import { Typography } from '@mui/material'
import React from 'react'

function NoDataFound() {
  return (
   <Typography sx={{width:'100vw'}}>No Boards found</Typography>
  )
}

export default NoDataFound
